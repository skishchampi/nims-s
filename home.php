<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once("includes/connection.php") ?>
    <?php require_once("includes/functions.php") ?>
    <meta charset="utf-8">
    <title>NGO Information Management Suite</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    

      #basicMap {
          width: 1000px;
          height: 500px;
          margin: 0;
      }

      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
  </head>

  <body >
    
     <?php
		$fp= fopen("xml/test.txt", "w");
	    $query="SELECT * from socialmap";
        $result=mysql_query($query, $connection);
        confirm_query($result);
		while($row=mysql_fetch_array($result)){
			$lat=$row['latitude'];
			$lon=$row['longitude'];
			$typ=$row['type'];
			$nam=$row['Name'];
			$vil=$row['village'];
			$a=$lat . "\n" . $lon . "\n" . $typ . "\n" . $nam . "\n" . $vil . "\n";
			fwrite($fp,$a);
		}
	?>
	<label>select district name : </label>
		
        <select id="jump" onchange="jump();">
            <option value="72.628112793,23.1886348724,7">=== Select district ===</option>
            <option value="70.7800,22.3000,11">Rajkot</option>
			<option value="72.3918352,23.5976589,11">Mahesana</option>
			<option value="72.1191325,23.8507063,11">Patan</option>
			<option value="72.13717,24.3964887,10">Banaskantha</option>
			<option value="73.2633572,23.9772603,11">Sabarkantha</option>
            <option value="72.6800,23.2200,12">gandhinagar</option>  
            <option value="72.6167,23.0333,11">Ahmedabad</option>
        </select>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="home.php">NIMS</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="communities.php">Communities</a></li>
              <li><a href="coordinator.php">Coordinators</a></li>
               <li><a href="projects.php">Projects</a></li>
              <li><a href="schools.php">Schools</a></li>
                <li><a href="settings.php">Settings</a></li>
		<li><a href="help.php">Help</a></li>
		<li><a href="logout.php">Logout</a></li>
            </ul>
            <p class="navbar-text pull-right">Logged in as <a href="#">Admin</a></p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span10">
          
          <div id="basicMap"> </div>    <!--/span-->
          
        </div>
        <div class="span2">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Updates</li>
              <li class="active"><a href="#">2 new map locations added on the map</a></li>
              <li><a href="#">Bhavaiya community information edited by Suresh</a></li>
              <li><a href="#">Projects information edited by admin</a></li>
              <li><a href="#">A new coordinator has been added to the database</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Company 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
       <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
    <script src="http://maps.google.com/maps/api/js?v=3.2&sensor=false"></script>
    <script src="map2.js"></script>

  </body>
</html>
